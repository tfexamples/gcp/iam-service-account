module "service_accounts" {
  source     = "terraform-google-modules/service-accounts/google"
  version    = "~> 4.1.0"
  project_id = var.project_id
  #prefix     = var.prefix
  names         = [var.prefix]
  description   = "Terraform Service Account"
  generate_keys = true
  project_roles = [
    "${var.project_id}=>roles/compute.admin",
    #"${var.project_id}=>roles/iam.serviceAccountUser",
    "${var.project_id}=>roles/resourcemanager.projectIamAdmin",
    "${var.project_id}=>roles/container.admin",
    "${var.project_id}=>roles/iam.serviceAccountAdmin",
    "${var.project_id}=>roles/iam.serviceAccountKeyAdmin"
  ]
  #display_name = "Single Account"
}
