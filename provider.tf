terraform {
  required_version = ">= 0.15.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.36.0"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 4.36.0"
    }
  }
}

provider "google" {
  credentials = file(var.credentials)
  project     = var.project_id
}

provider "google-beta" {
  credentials = file(var.credentials)
  project     = var.project_id
}
